# -*- coding: utf-8 -*-

from django.views.generic.base import TemplateView, View
from django.views.generic.list import ListView

class HomeView(TemplateView):
    template_name = 'home.html'

    def get(self, request, *args, **kwargs):
        return super(HomeView, self).get(self.request, *args, **kwargs)

    def get_context_data(self, *args, **kwargs):
        context = super(HomeView, self).get_context_data(*args, **kwargs)

        return context

