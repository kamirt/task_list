# -*- coding: utf-8 -*-
from django.db import models
from django.db.models import signals as signals
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.models import Group

# this function tries to create superuser after the first migration
# helpful for development

def create_admin_user(app_config, **kwargs):
    if app_config.name != 'core':
        return None

    try:
        CoreUser.objects.get(username='admin')
    except CoreUser.DoesNotExist:
        print('Creating admin user: login: admin, password: 123')
        assert CoreUser.objects.create_superuser('admin', 'admin@localhost', '123')
    else:
        print('Admin user already exists')


signals.post_migrate.connect(create_admin_user)

# Abstract model to make models with time added and updated
class TimeStampedModel(models.Model):
    date_added = models.DateTimeField(auto_now_add=True, db_index=True, verbose_name='Дата добавления')
    date_updated = models.DateTimeField(auto_now=True, db_index=True, verbose_name='Дата обновления')

    class Meta(object):
        abstract = True

# Just adds 'order' field to your model, abstract
class OrderedModel(models.Model):
    order = models.PositiveIntegerField(default=0, verbose_name='Порядок')

    class Meta(object):
        abstract = True
        ordering = ['order']

class TaskGroupManager(models.Manager):
    def get_query_set(self):
        return super(TaskGroupManager, self).get_query_set().filter(coreuser__enrolled=True).distinct()

class TaskGroup(Group):
    objects = models.Manager()
    has_users = TaskGroupManager()

    class Meta:
        verbose_name_plural = 'Группы задач'
        ordering = ['name']

    def __unicode__(self):
        return u'%s' % (self.name)


# Custom user
class CoreUser(AbstractUser):
    task_group = models.ForeignKey(TaskGroup, verbose_name='Группа задач', related_name='users', null=True, blank=True)

