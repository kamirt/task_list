# -*- coding: utf-8 -*-
from django.contrib import admin
from django.contrib.admin.options import ModelAdmin
from .models import CoreUser, TaskGroup


@admin.register(CoreUser)
class CoreUserAdmin(ModelAdmin):
    pass
@admin.register(TaskGroup)
class TaskGroupAdmin(ModelAdmin):
    pass