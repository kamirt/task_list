# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('django_task_list', '0001_initial'),
        ('auth', '0006_require_contenttypes_0002'),
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='taskgroup',
            name='tasks',
            field=models.ManyToManyField(to='django_task_list.Task', verbose_name=b'\xd0\x97\xd0\xb0\xd0\xb4\xd0\xb0\xd1\x87\xd0\xb8 \xd0\xb4\xd0\xbb\xd1\x8f \xd0\xb3\xd1\x80\xd1\x83\xd0\xbf\xd0\xbf\xd1\x8b', blank=True),
        ),
        migrations.AddField(
            model_name='coreuser',
            name='groups',
            field=models.ManyToManyField(related_query_name='user', related_name='user_set', to='auth.Group', blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', verbose_name='groups'),
        ),
        migrations.AddField(
            model_name='coreuser',
            name='user_permissions',
            field=models.ManyToManyField(related_query_name='user', related_name='user_set', to='auth.Permission', blank=True, help_text='Specific permissions for this user.', verbose_name='user permissions'),
        ),
    ]
