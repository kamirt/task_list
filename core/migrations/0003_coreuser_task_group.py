# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_auto_20170430_1449'),
    ]

    operations = [
        migrations.AddField(
            model_name='coreuser',
            name='task_group',
            field=models.ForeignKey(related_name='users', verbose_name=b'\xd0\x93\xd1\x80\xd1\x83\xd0\xbf\xd0\xbf\xd0\xb0 \xd0\xb7\xd0\xb0\xd0\xb4\xd0\xb0\xd1\x87', blank=True, to='core.TaskGroup', null=True),
        ),
    ]
