# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_coreuser_task_group'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='taskgroup',
            name='tasks',
        ),
    ]
