from django.conf.urls import patterns, include, url
from django.utils.encoding import iri_to_uri
from django.conf import settings
from django.contrib import admin


admin.autodiscover()

urlpatterns = patterns('',
    url(r'^', include('core.urls')),
	url(r'^django-task-list/', include('django_task_list.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/login/$', 'django.contrib.auth.views.login', {'template_name': 'login.html'}),
    url(r'^logout/$', 'django.contrib.auth.views.logout_then_login', name='logout'),
)
if settings.DEBUG:
    media_url = settings.MEDIA_URL[1:] if settings.MEDIA_URL.startswith('/') \
        else settings.MEDIA_URL

    urlpatterns += patterns(
        '',
        (r'^{0}(?P<path>.*)$'.format(iri_to_uri(media_url)),
         'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}
        ),
    )