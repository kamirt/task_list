# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings
import django_task_list.models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='FlaggedTask',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_added', models.DateTimeField(auto_now_add=True, verbose_name=b'\xd0\x94\xd0\xb0\xd1\x82\xd0\xb0 \xd0\xb4\xd0\xbe\xd0\xb1\xd0\xb0\xd0\xb2\xd0\xbb\xd0\xb5\xd0\xbd\xd0\xb8\xd1\x8f', db_index=True)),
                ('date_updated', models.DateTimeField(auto_now=True, verbose_name=b'\xd0\x94\xd0\xb0\xd1\x82\xd0\xb0 \xd0\xbe\xd0\xb1\xd0\xbd\xd0\xbe\xd0\xb2\xd0\xbb\xd0\xb5\xd0\xbd\xd0\xb8\xd1\x8f', db_index=True)),
                ('order', models.PositiveIntegerField(default=0, verbose_name=b'\xd0\x9f\xd0\xbe\xd1\x80\xd1\x8f\xd0\xb4\xd0\xbe\xd0\xba')),
                ('finish_time', models.DateTimeField(verbose_name=b'\xd0\x92\xd1\x80\xd0\xb5\xd0\xbc\xd1\x8f \xd0\xb7\xd0\xb0\xd0\xb2\xd0\xb5\xd1\x80\xd1\x88\xd0\xb5\xd0\xbd\xd0\xb8\xd1\x8f \xd0\xb7\xd0\xb0\xd0\xb4\xd0\xb0\xd1\x87\xd0\xb8')),
            ],
            options={
                'ordering': ['finish_time'],
                'verbose_name': '\u043f\u0435\u0440\u0435\u0434\u0430\u043d\u043d\u0430\u044f \u0437\u0430\u0434\u0430\u0447\u0430',
                'verbose_name_plural': '\u041f\u0435\u0440\u0435\u0434\u0430\u043d\u043d\u044b\u0435 \u0437\u0430\u0434\u0430\u0447\u0438',
            },
        ),
        migrations.CreateModel(
            name='Task',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('priority', django_task_list.models.RangeField(default=0, verbose_name=b'\xd0\x9f\xd1\x80\xd0\xb8\xd0\xbe\xd1\x80\xd0\xb8\xd1\x82\xd0\xb5\xd1\x82')),
                ('title', models.CharField(default=b'\xd0\x91\xd0\xb5\xd0\xb7 \xd0\xb8\xd0\xbc\xd0\xb5\xd0\xbd\xd0\xb8', max_length=300, verbose_name=b'\xd0\x9d\xd0\xb0\xd0\xb7\xd0\xb2\xd0\xb0\xd0\xbd\xd0\xb8\xd0\xb5 \xd0\xb7\xd0\xb0\xd0\xb4\xd0\xb0\xd1\x87\xd0\xb8')),
                ('description', models.TextField(verbose_name=b'\xd0\x9e\xd0\xbf\xd0\xb8\xd1\x81\xd0\xb0\xd0\xbd\xd0\xb8\xd0\xb5 \xd0\xb7\xd0\xb0\xd0\xb4\xd0\xb0\xd1\x87\xd0\xb8')),
            ],
            options={
                'ordering': ['priority'],
                'verbose_name': '\u0437\u0430\u0434\u0430\u0447\u0430',
                'verbose_name_plural': '\u0417\u0430\u0434\u0430\u0447\u0438',
            },
        ),
        migrations.AddField(
            model_name='flaggedtask',
            name='task',
            field=models.ForeignKey(verbose_name=b'\xd0\x97\xd0\xb0\xd0\xb4\xd0\xb0\xd1\x87\xd0\xb0', to='django_task_list.Task'),
        ),
        migrations.AddField(
            model_name='flaggedtask',
            name='user',
            field=models.ForeignKey(verbose_name=b'\xd0\x9a\xd0\xbe\xd0\xbc\xd1\x83 \xd0\xbd\xd0\xb0\xd0\xb7\xd0\xbd\xd0\xb0\xd1\x87\xd0\xb5\xd0\xbd\xd0\xb0', to=settings.AUTH_USER_MODEL),
        ),
    ]
