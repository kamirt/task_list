# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0004_remove_taskgroup_tasks'),
        ('django_task_list', '0002_auto_20170430_1842'),
    ]

    operations = [
        migrations.AddField(
            model_name='task',
            name='group',
            field=models.ManyToManyField(to='core.TaskGroup', verbose_name=b'\xd0\x94\xd0\xbb\xd1\x8f \xd0\xb3\xd1\x80\xd1\x83\xd0\xbf\xd0\xbf\xd1\x8b', blank=True),
        ),
    ]
