from __future__ import print_function
import time
import datetime

from django.core.management.base import BaseCommand, CommandError
from django.utils.timezone import utc
from django.conf import settings


from django.db import transaction

import datetime
import os.path
import sys


class Command(BaseCommand):

    help = 'This command is the same as populate_from_users'

    def handle(self, *args, **kwargs):
        pass
        """
        # Let's take a users one by one and get their medias and information
        priority_update = False
        print('starting main loop...')
        while True:
            api = create_api()
            with transaction.atomic():
                if priority_update:
                    insta_user = InstagramUser.objects.filter(is_locked=False, is_private=False, follower_count__gte=config.POPULAR_FOLLOWERS_COUNT).order_by('date_updated').first()
                else:
                    insta_user = InstagramUser.objects.filter(is_locked=False, is_private=False).order_by('date_updated').first()

                if insta_user:
                    print('Locking user account {0}... '.format(insta_user.username), end='')
                    insta_user.is_locked = True
                    insta_user.save()
                    print('done')
                else:
                    self.stdout.write('Nothing to parse')
                    time.sleep(30)
                    priority_update = False
                    continue

            try:
                self.stdout.write('Scraping instagram user {0}'.format(insta_user.username))
                user_info = api.user_info(insta_user.user_id)


            except:
                self.stdout.write('Failed to fetch user info {0}'.format(insta_user.username))
                continue
            print('updating user information...', end='')
            InstagramUser.objects.filter(pk=insta_user.pk).update(
                fullname=user_info['full_name'] or ' ',
                biography=user_info['biography'] or ' ',
                hd_profile_pic_url=user_info['profile_pic_url'].replace('/s150x150', ''),
                media_count=user_info['media']['count'],
                is_private=user_info['is_private'],
                follower_count=user_info['followed_by']['count'],
                following=user_info['follows']['count']
            )
            print('success')
            if user_info['is_private']:
                self.stdout.write('User {0} is private, pass'.format(insta_user.username))
                continue

            # fetching data
            
            more_available = True
            num_results = 0
            next_max_id = ''

            while more_available and num_results < config.MAX_MEDIA_COUNT:
                print('fetching data, circle %s' % str(num_results))


                if next_max_id:
                    results = api.user_feed(insta_user.user_id, end_cursor=next_max_id)
                else:
                    results = api.user_feed(insta_user.user_id)

                next_max_id = results['end_cursor'] if results['end_cursor'] != next_max_id else None
                print('data has been received!')
                for media in results['info']:
                    sys.stdout.write('Scraping media {0}\n'.format(media['id']))
                    media_info = api.media_info2(media['code'])
                    try:
                        view_count = str(media_info['video_views'])
                        video_url = media_info['video_url']
                    except:
                        view_count = ''
                        video_url = ''

                    media_time = media['date']
                    print('getting caption and hashtags...', end='')
                    try:
                        caption = media_info['caption']
                        hashtags = populate_function.hashtag_parser(media_info['caption'])
                        for t in hashtags:
                            try:
                                Tag.objects.get(name=t)
                            except Tag.DoesNotExist:
                                Tag.objects.create(name=t)
                            except:
                                continue
                        print('success')
                    except:
                        print('error, leave them blank')
                        caption = " "

                    print('thumbnail_src: ', media['thumbnail_src'])
                    print('display_src: ', media['display_src'])
                    if media_info['is_video']:
                        print('video_small and big', media_info['video_url'])
                        print('video view count', media_info['video_views'])
                    print('url', 'https://www.instagram.com/p/' + media_info['code'])
                    print('hash', media_info['code'])
                    print('like_count', media['likes']['count'])
                    print('comment_count', media['comments']['count'])

                    final_comments = []
                    print('getting comments...', end='')
                    comments = api.media_comments(media_info['code'], count = int(config.COUNT_OF_COMMENTS))
                    for comment in comments:
                        final_comments.append({
                            'text': comment['text'],
                            'username': comment['user']['username'],
                            'profile_pic_url': comment['user']['profile_pic_url'],
                            })
                    print('success')
                    final_likers = []
                    if media['likes']['count'] > 0:
                        print('getting likes...', end='')
                        for user in media_info['likes']['nodes']:
                            with transaction.atomic():
                                try:
                                    instauser = InstagramUser.objects.get(user_id=user['user']['id'])
                                except InstagramUser.DoesNotExist:
                                    instauser = InstagramUser.objects.create(
                                        user_id=user['user']['id'],
                                        profile_pic_url = user['user']['profile_pic_url'],
                                        username = user['user']['username'],
                                        )
                            final_likers.append(instauser) 
                        print('success')
                    print('trying to find media with media_id...', end='')
                    with transaction.atomic():
                        try: 
                            m = Media.objects.get(media_id=int(media_info['id']))
                            m.media_data.comments = final_comments
                            m.save()
                            print('success')
                        except Media.DoesNotExist:
                            print(' ')
                            print('media does not exists, creating MediaData object...', end='')
                            md = MediaData.objects.create(
                                data = media,
                                comments = final_comments
                                )

                            print('adding likers...', end='')
                            for liker in final_likers:
                                md.likers.add(liker)
                            print('success')

                            sys.stdout.write('Media {0} has {1} comments\n'.format(media['id'], media['comments']['count']))
                            sys.stdout.write('Media {0} has {1} likes\n'.format(media['id'], media['likes']['count']))

                            print('trying to create Media object...', end='')
                            first_substr = media['thumbnail_src'][:53]
                            second_substr = media['thumbnail_src'][61:]
                            size = 's150x150'
                            thumbnail = first_substr + size + second_substr
                            Media.objects.create(
                                media_id = media['id'],
                                view_count = view_count,
                                from_user = insta_user,
                                duration = '',
                                description = caption,
                                pic_preview = thumbnail,
                                pic_max = media['display_src'],
                                pic = media['thumbnail_src'],
                                video_small = video_url,
                                video_big = video_url,
                                url = 'https://www.instagram.com/p/' + media_info['code'],
                                hash = media_info['code'],
                                like_count = media['likes']['count'],
                                comment_count = media['comments']['count'],
                                device_timestamp = str(media_time),
                                taken_at = datetime.datetime.utcfromtimestamp(float(str(media_time))).replace(tzinfo=utc),
                                media_data = md
                                )
                            print('success')
                    num_results += 1   
                    if num_results >= config.MAX_MEDIA_COUNT:
                        print('information collected with overhead, unlocking user...', end='')
                        InstagramUser.objects.filter(pk=insta_user.pk).update(is_locked=False)
                        print('success')
                        break
                if not next_max_id:
                    print('information collected, no more available, unlocking user...', end='')
                    InstagramUser.objects.filter(pk=insta_user.pk).update(is_locked=False)
                    print('success')
                    break
            print('information collected normal, unlocking user...', end='')
            InstagramUser.objects.filter(pk=insta_user.pk).update(is_locked=False)
            priority_update = not priority_update
            print('success')




"""