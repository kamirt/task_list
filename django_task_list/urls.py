# -*- coding: utf-8 -*-
from django.conf.urls import include, patterns, url
from django.contrib.auth.decorators import login_required


from .views import *

urlpatterns = [
    url(r'^$', login_required(TasksView.as_view()), name='task_list'), 
    url(r'^send-task/$', login_required(AjaxTasksView.as_view()), name='task_done_url'), 
]