from .models import Task, FlaggedTask
from core.models import TaskGroup, CoreUser

from celery import shared_task
from celery import Celery
from celery.schedules import crontab

app = Celery()

@app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):

    sender.add_periodic_task(
        crontab(minute=1),
        create_daily_tasks.s(),
    )



# turn task 'priority' field 
# into flagged task certain 'order' by indexing

def fill_by_priority(task_list, user_list, users_count, tasks_count, incrementor):
    main_loop = tasks_count//users_count

    second_loop = tasks_count%users_count

    iterator = 0
    if main_loop:
        for i in range(main_loop):
            for user in user_list:
                print user.username
                try:
                    FlaggedTask.objects.create(
                        task=task_list[iterator], 
                        order=incrementor+i+1,
                        user=user
                        )
                    iterator+=1
                except IndexError:
                    pass

    if second_loop:
        main_loop+=1
        for user in user_list:
            try:
                FlaggedTask.objects.create(
                    task=task_list[iterator], 
                    order=incrementor+main_loop,
                    user=user
                    )  
                iterator+=1
            except IndexError:
                pass
            
    return main_loop


@shared_task
def create_daily_tasks():
    # taking all of the fulfilled groups
    groups = TaskGroup.has_users.all()

    for group in groups:
        # dividing our tasks by priority
        low_priority_tasks = group.task.filter(priority__gte=1).filter(priority__lte=3)
        medium_priority_tasks = group.task.filter(priority__gt=3).filter(priority__lte=6)
        high_priority_tasks = group.task.filter(priority__gt=6)

        users = group.users.all()
        users_count = users.count()

        # this gives one task per priority to user with order=1
        # then, if tasks_count more than users
        # it takes them another one with the same priority with order=2, etc...

        high = fill_by_priority(high_priority_tasks, users, users_count, high_priority_tasks.count(), 0)
        medium = fill_by_priority(medium_priority_tasks, users, users_count, medium_priority_tasks.count(), high)
        low = fill_by_priority(low_priority_tasks, users, users_count, low_priority_tasks.count(), medium+high)
