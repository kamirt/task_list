# -*- coding: utf-8 -*-
from django.db import models
from core.models import TimeStampedModel, OrderedModel, CoreUser


class RangeField(models.IntegerField):
    def __init__(self, verbose_name=None, name=None, min_value=None, max_value=None, **kwargs):
        self.min_value, self.max_value = min_value, max_value
        models.IntegerField.__init__(self, verbose_name, name, **kwargs)
    def formfield(self, **kwargs):
        defaults = {'min_value': self.min_value, 'max_value':self.max_value}
        defaults.update(kwargs)
        return super(RangeField, self).formfield(**defaults)

class Task(models.Model):
    group = models.ManyToManyField('core.TaskGroup', verbose_name='Для группы', blank=True, related_name='task')
    priority = RangeField(min_value=0, max_value=10, default=0, verbose_name='Приоритет')
    title = models.CharField(max_length=300, verbose_name='Название задачи', default='Без имени')
    description = models.TextField(verbose_name='Описание задачи')
    class Meta:
        verbose_name = 'задача'
        verbose_name_plural = 'Задачи'
        ordering = ['priority']

    def __unicode__(self):
        return u'%s' % (self.title)



class FlaggedTask(OrderedModel, TimeStampedModel):
    task = models.ForeignKey(Task, verbose_name='Задача', on_delete=models.PROTECT)
    finish_time = models.DateTimeField(verbose_name='Время завершения задачи', blank=True, null=True)
    user = models.ForeignKey(CoreUser, verbose_name='Кому назначена')

    class Meta:
        verbose_name = 'переданная задача'
        verbose_name_plural = 'Переданные задачи'
        ordering = ['finish_time']

    def __unicode__(self):
        return u'%s' % (self.task.title)
