# -*- coding: utf-8 -*-
from django.template.context import RequestContext
from django.template.loader import render_to_string
from django.http import HttpResponse
from django.views.generic.base import TemplateView, View
from django.views.generic.list import ListView
from datetime import date
from datetime import datetime

from .models import FlaggedTask

import pytz
import json

class TasksView(TemplateView):
    template_name = 'task-list.html'

    def get(self, request, *args, **kwargs):
        return super(TasksView, self).get(self.request, *args, **kwargs)

    def get_context_data(self, *args, **kwargs):
        context = super(TasksView, self).get_context_data(*args, **kwargs)
        user = self.request.user
        task_list = FlaggedTask.objects.filter(user=user,
            date_added__day=date.today().day,
            date_added__month=date.today().month,
            date_added__year=date.today().year).order_by('order')
        first_undone = task_list.filter(finish_time=None).first()
        if first_undone:
            context['first_order'] = first_undone.order
        context['task_list'] = task_list

        return context

class AjaxTasksView(View):
    template_name = 'task-list.html'

    def get(self, request, *args, **kwargs):
        return HttpResponse(status=404)

    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            data=json.loads(request.body)
            task_pk=data.get('task', None)
            if task_pk:
                task = FlaggedTask.objects.get(pk=task_pk)
                t = datetime.utcnow()
                t = t.replace(tzinfo=pytz.utc)
                task.finish_time = t
                task.finish_time=datetime.now()
                task.save()
            
                context = {
                    'details': render_to_string('done.html', locals()),
                }
                data = json.dumps(context)
                response_kwargs = {}
                response_kwargs['content_type'] = 'application/json'
                return HttpResponse(data, **response_kwargs)

        return HttpResponse(status=404)