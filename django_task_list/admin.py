# -*- coding: utf-8 -*-
from django.contrib import admin
from django.contrib.admin.options import ModelAdmin
from .models import Task, FlaggedTask


@admin.register(Task)
class TaskAdmin(ModelAdmin):
    pass
@admin.register(FlaggedTask)
class FlaggedTaskAdmin(ModelAdmin):
    list_filter = ('finish_time',)

